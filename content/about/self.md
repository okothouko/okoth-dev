---
title: "About Self"
date: 2022-01-31T00:36:51+03:00
draft: false
blog: false
---
# Profile

Self-taught developer, motivated by data and results, with  experience in both front-end and back-end development stacks . I enjoy creative problem solving and getting exposure on   multiple projects.

# Key Intrests

1. Software Development
2. DevOps
3. Data Engineering

# Skills

**Programming:**
> Python(Django, Flask) JavaScript(React, Node js) SQL, Git, Linux

**Software Development,Solutions Deployment:**
> Coding & Scripting, Debugging & Troubleshooting

**Data and Analytics Tools/Packages:**
> Jupyter Notebook, Apache-Spark, R Studio, Excel, Power Bi, Tableau

**Big Data & Cloud:**
> Azure, Google BigQuery, data studio, Amazon S3, Spark

**Statistical/Machine learning:**
> Linear/Logistic regression, SVM, Random Forests, Clustering, Gradient Boosted  trees